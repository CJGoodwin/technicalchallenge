# AccountManagementService::RelatedCollectionACCSCapPercentage

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ACCSCapPercentage&gt;**](ACCSCapPercentage.md) |  | [optional] 


