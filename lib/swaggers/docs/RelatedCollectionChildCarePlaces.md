# AccountManagementService::RelatedCollectionChildCarePlaces

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ChildCarePlaces&gt;**](ChildCarePlaces.md) |  | [optional] 


