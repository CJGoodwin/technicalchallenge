# AccountManagementService::RelatedCollectionAccountManagementServiceName

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ServiceName&gt;**](ServiceName.md) |  | [optional] 


