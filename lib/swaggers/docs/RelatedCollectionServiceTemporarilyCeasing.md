# AccountManagementService::RelatedCollectionServiceTemporarilyCeasing

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ServiceTemporarilyCeasing&gt;**](ServiceTemporarilyCeasing.md) |  | [optional] 


