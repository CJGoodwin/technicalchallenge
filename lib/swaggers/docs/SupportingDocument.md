# AccountManagementService::SupportingDocument

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**document_type** | **String** | Document Type | [optional] 
**file_name** | **String** | File Name | [optional] 
**mime_type** | **String** | MIME Type | [optional] 
**file_content** | **String** | File Content | [optional] 


