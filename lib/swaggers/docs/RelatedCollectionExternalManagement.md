# AccountManagementService::RelatedCollectionExternalManagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ExternalManagement&gt;**](ExternalManagement.md) |  | [optional] 


