# AccountManagementService::ExternalManagement

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**start_date** | **String** | Start Date for the update | [optional] 
**end_date** | **String** | End Date for the update | [optional] 
**action** | **String** | Action for this External Management | [optional] 
**type** | **String** | Service External Management type | [optional] 
**name** | **String** | Service External Management Name | [optional] 
**first_name** | **String** | Service External Management First Name | [optional] 
**last_name** | **String** | Service External Management Last Name | [optional] 
**abn** | **String** | Service External Management ABN | [optional] 
**abr_name** | **String** | External Management Name as stored by the ABR | [optional] 
**acn** | **String** | External Management ACN | [optional] 
**email** | **String** | External Management email address | [optional] 
**phone** | **String** | External Management telephone number | [optional] 
**arrangement** | **String** | Details of the arrangement Service will have in place | [optional] 
**address** | [**RelatedCollectionAddress**](RelatedCollectionAddress.md) |  | [optional] 


