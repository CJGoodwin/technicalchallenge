# AccountManagementService::RelatedCollectionContact

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;Contact&gt;**](Contact.md) |  | [optional] 


