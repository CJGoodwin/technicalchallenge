# AccountManagementService::RelatedCollectionAgency

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;Agency&gt;**](Agency.md) |  | [optional] 


