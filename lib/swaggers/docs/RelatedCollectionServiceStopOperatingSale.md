# AccountManagementService::RelatedCollectionServiceStopOperatingSale

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;ServiceStopOperatingSale&gt;**](ServiceStopOperatingSale.md) |  | [optional] 


