# AccountManagementService::RelatedCollectionPersonnel

## Properties
Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**results** | [**Array&lt;Personnel&gt;**](Personnel.md) |  | [optional] 


